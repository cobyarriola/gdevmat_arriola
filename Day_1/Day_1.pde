void setup() // Gets called when the program runs
{
  size(1920, 1080, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30/ 180), 
    0, 0, 0, 
    0, -1, 0);
  w = width + 16;
  dx = (TWO_PI / period) * xspacing;
  yvalues = new float[w/xspacing];
}

void draw() // Gets called every frame
{
  background(255);
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  calcWave();
  renderWave();
}

void drawCartesianPlane()
{
  line(300, 0, -300, 0);
  line(0, 300, 0, -300);

  for (int i = -300; i <= 300; i+=10)
  {
    line(i, -5, i, 5);
    line(-5, i, 5, i);
  }
}


void drawLinearFunction()
{
  /*
  f(x) = x + 2
   let x be 4, then y = 6  (4, 6)
   let X be -5, then Y = -3  (-5, 3)
   */

  for (int x = -200; x <= 200; x++)
  {
    circle(x, x + 2, 1);
  }
}


void drawQuadraticFunction()
{
  /*
   f(x) = x^2 + 2x - 5
   Let x be 2, then y = 3
   Let x be -2, then y = 3
   Let x be -1, then y = -6
   */

  for (float x = -300; x <= 300; x+=0.1)
  {
    circle(x * 10, (x * x) + (2 * x) - 5, 1);
  }
}

float radius = 50;


void drawCircle()
{
  for (int x = 0; x < 360; x++)
  {
    circle((float)Math.cos(x) * radius, (float)Math.sin(x)* radius, 1);
  }
}

int xspacing = 16;   // How far apart should each horizontal location be spaced
int w;              // Width of entire wave

float theta = 0.0;  // Start angle at 0
float amplitude = 75.0;  // Height of wave
float period = 500.0;  // How many pixels before the wave repeats
float dx;  // Value for incrementing X, a function of period and xspacing
float[] yvalues;  // Using an array to store height values for the wave

void calcWave() {
  // Increment theta (try different values for 'angular velocity' here
  theta += 0.02;

  // For every x value, calculate a y value with sine function
  float x = theta;
  for (int i = 0; i < yvalues.length; i++) {
    yvalues[i] = sin(x)*amplitude;
    x+=dx;
  }
}

void renderWave()
{
  noStroke();
  fill(0);
  // A simple way to draw the wave with an ellipse at each location
  for (int x = 0; x < yvalues.length; x++) {
    ellipse(x*xspacing, height/2+yvalues[x], 16, 16);
}
}
