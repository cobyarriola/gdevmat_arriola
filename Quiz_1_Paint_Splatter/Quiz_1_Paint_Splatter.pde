void setup()
{
  size (1280, 720, P3D);

  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);

  background(255);
}

float generateSize(float value)
{
  float sizeGen = randomGaussian();
  float standDevSize = value;
  float meanSize = 2;
  float ellipseSize;
  ellipseSize = standDevSize * sizeGen + meanSize;

  return ellipseSize;
}

float gaussianBell(float value)
{
  float num = randomGaussian();
  float standardDeviation = value;
  float mean = 65;
  float x = standardDeviation * num + mean;

  return x;
}

void draw()
{ 

  float y = random(-height, height);
  float gaussian;
  float sizeValue;

  gaussian = gaussianBell(150);
  sizeValue = generateSize(30);

  noStroke();
  ellipse(gaussian, y, sizeValue, sizeValue);
  fillColor();

  if (frameCount%1000 == 0)
  {
    background(255);
  }
}



void fillColor()
{
  float R = random(256); 
  float G = random(256);
  float B = random(256);
  float A = random(256);

  fill(R, G, B, A);
}
