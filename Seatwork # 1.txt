void setup()
{
  size(960,720,P3D);
  camera(0,0,-(height/2)/tan(PI*30/180),
  0,0,0,
  0,-1,0);
  background(130);
 
}

Walker walker = new Walker();

void draw()
{

  walker.walk();
  walker.render();
}

class Walker
{
  float xPosition;
  float yPosition;
  
  Walker()
  {
    xPosition = 0;
    yPosition=0;
  }
  
  Walker(float x,float y)
  {
    xPosition = x;
    yPosition = y;
  }
  
  void render()
  {
      circle(xPosition,yPosition,30);

      float R = random(256);
      float G = random(256);
      float B = random(256); 
      fill(R, G, B);
  }
  
  void walk()
  {
    int direction = floor(random(8));
    
    if (direction==0)
    {
      yPosition+= 10;
    }
    else if(direction==1)
    {
      yPosition-=10;
    }
    else if (direction==2)
    {
     xPosition+=10; 
    } 
    else if (direction==3)
    {
      xPosition-=10;
    }
    else if(direction==4)
    {
     xPosition+=10;
     yPosition+=10;
    } 
    else if(direction==5)
    {
     xPosition+=10;
     yPosition-=10;
    }
     else if(direction==6)
    {
     xPosition-=10;
     yPosition-=10;
    }
     else if(direction==7)
    {
     xPosition-=10;
     yPosition+=10;
    }
  }
}